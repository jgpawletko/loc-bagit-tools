#!/bin/bash

# NOTE: assumes that this will be run from project root
#       e.g., $ ./test/test.sh

function do_test {
    cmd=$1
    expected_result=$2

    output=$( $cmd 2>&1 )
    result=$?
    if [[ "$result" -eq "$expected_result" ]]
    then
        echo "PASS  : $cmd"
    else
        echo "FAIL  : $cmd"
        echo "      RESULT: $result"
        echo "      OUTPUT: $output"
    fi
}


# test invalid case
do_test './bin/batch-bag verifyvalid ./test/fixtures/valid*' 0
do_test './bin/batch-bag verifyvalid ./test/fixtures/invalid*' 1

