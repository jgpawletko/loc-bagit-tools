Source-organization: DLTS
Organization-address: 70 Washington Square South, New York, NY 10012
Contact-name: Melitte Buchman
Contact-phone: +1-212-998-2668
Contact-email: melitte@nyu.edu
Bagging-date: 2014-05-27T19:52:18Z
Payload-Oxum: 20.1
Internal-sender-identifier: tamwag/oh009
Internal-sender-description: United Federation of Teachers Oral History Collection
nyu-dl-project-name: tamwag/oh009
nyu-dl-hostname: dl-odin.local
nyu-dl-pathname: /Users/jgp/Desktop/vanilla
